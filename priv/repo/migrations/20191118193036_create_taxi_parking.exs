defmodule TaxiParking.Repo.Migrations.CreateTaxiParking do
  use Ecto.Migration

  def change do
    create table(:taxi_parking) do
      add :name, :string
      add :username, :string
      add :hashed_password, :string
      add :email, :string
      add :credit, :float
      timestamps()
    end

  end
end
