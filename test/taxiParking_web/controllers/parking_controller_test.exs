defmodule TaxiParkingWeb.PageControllerTest do
  use TaxiParkingWeb.ConnCase
  alias TaxiParking.Repo
  alias TaxiParking.Taxi.Parking

#  test "GET /", %{conn: conn} do
#    conn = get conn, "/parking"
#    assert html_response(conn, 200) =~ "Welcome to Phoenix!"
#  end
@user_parm %{
  "credit" => "123",
  "email" => "shobit@ut.ee",
  "hashed_password" => "qweqwewq",
  "name" => "Shobitqweqwe Jain",
  "username" => "qweqwewq123"
}
  test "User register success", %{conn: conn} do

  conn = build_conn()|>post("/parking", %{"parking" => %{
    "credit" => "123",
    "email" => "shobit@ut.ee",
    "hashed_password" => "qweqwewq",
    "name" => "Shobitqweqwe Jain",
    "username" => "qweqwewq123"
  }})

    response = html_response(conn, 302)
    result = Repo.all(Parking)
    assert conn.status == 302
    assert length(result) == 1
    assert conn.status == 302

   end

  test "Registration submission with failure", %{conn: conn} do
    conn = get conn, "/parking", %{parking: [hashed_password: "parool", email: "abc@gmail.com", credit: "2.1", monthly_selected: false]}
    res = html_response(conn, 200)
    result = Repo.all(Parking)
    assert length(result) == 0
  end
end
