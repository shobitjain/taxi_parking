defmodule TaxiParkingWeb.ParkingController do
  use TaxiParkingWeb, :controller
  alias TaxiParking.Repo
  alias TaxiParking.Taxi.Parking

  def index(conn, _params) do
    users = Repo.all(Parking)
    render(conn, "index.html", users: users)
  end

  def new(conn, _params) do
    changeset = Parking.changeset(%Parking{}, %{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"parking" => user_params}) do
    IO.puts("======================")
    IO.inspect(user_params)
    IO.puts("======================")
    changeset = Parking.changeset(%Parking{}, user_params)
    case Repo.insert(changeset) do
      {:ok, _post} ->
        conn
        |> put_flash(:info, "user registered")
        |> redirect(to: parking_path(conn, :index))
      {:error, changeset} -> render(conn, "new.html", changeset: changeset)
    #redirect(conn, to: parking_path(conn, :index))
    end
  end

  # def create(conn, params) do
  #   changeset = Parking.changeset(%Parking{}, params["parking"])
  #   case Repo.insert(changeset) do
  #     {:ok, _user} ->
  #       conn
  #       |> put_status(200)
  #       |> json(%{message: "user registered successfully."})
  #       #redirect(conn, to: parking_path(conn, :index))
  #     {:error, _} ->
  #       conn
  #       |> put_status(400)
  #       |> json(%{message: "Bad Data."})
  #   end
  # end
  #def edit()

  def edit(conn, %{"id" => user_params}) do
    parking = Repo.get(Parking, user_params )
    changeset = Parking.changeset(parking)
    render conn, "edit.html", changeset: changeset, parking: parking
  end

  def update(conn, %{"id"=>user_params, "parking"=>user_params1}) do
    old_info = Repo.get(Parking, user_params)
    changeset = Parking.changeset(old_info, user_params1)
    #changeset = Repo.get(Parking, user_params) |> Parking.changeset(user_params1)
    case Repo.update(changeset) do
      {:ok, _user} ->
        conn
        |> put_flash(:info, "Info Update")
        |> redirect(to: parking_path(conn, :index))
      {:error, changeset} -> render(conn, "edit.html", changeset: changeset, parking: old_info)
    end
  end
  def delete(conn, %{"id"=>user_params}) do
    Repo.get!(Parking,user_params)
    |> Repo.delete!
    conn
    |> put_flash(:info, "Info Deleted")
    |> redirect(to: parking_path(conn, :index))
  end
end
