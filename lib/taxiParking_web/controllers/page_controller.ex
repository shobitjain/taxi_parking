defmodule TaxiParkingWeb.PageController do
  use TaxiParkingWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
