defmodule TaxiParkingWeb.Router do
  use TaxiParkingWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug TaxiParking.Authentication, repo: TaxiParking.Repo
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", TaxiParkingWeb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    resources "/users", UserController, only: [:index, :new]
    #put "/parking/:id", ParkingController, :update
    resources "/parking", ParkingController
    get "/session", SessionController, :index
    #get "/parking/:id/edit", ParkingController, :edit
    #put "/parking/:id", ParkingController, :update

  end

  # Other scopes may use custom stacks.
  # scope "/api", TaxiParkingWeb do
  #   pipe_through :api
  # end
end
