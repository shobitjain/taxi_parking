defmodule TaxiParking.Taxi.Parking do
  use Ecto.Schema
  import Ecto.Changeset


  schema "taxi_parking" do
    field :credit, :float
    field :email, :string
    field :hashed_password, :string
    field :name, :string
    field :username, :string

    timestamps()
  end

  @doc false
  def changeset(parking, attrs) do
    parking
    |> cast(attrs, [:name, :username, :hashed_password, :email, :credit])
    |> validate_required([:name, :username, :hashed_password, :email, :credit])
  end
end
