# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     TaxiParking.Repo.insert!(%TaxiParking.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias TaxiParking.{Repo,Taxi.Parking}
#alias RepoName.{Repo,FolderName.FileName}
[%{name: "Fred Flintstone", username: "fred", hashed_password: "parool", email: "abc@gmail.com", credit: 2.1}]
|> Enum.map(fn taxi_parking_data -> Parking.changeset(%Parking{}, taxi_parking_data) end)
#|> Enum.map(fn TableName_data -> FileName.changeset(%FileName{}, TableName_data) end)
|> Enum.each(fn changeset -> Repo.insert!(changeset) end)
